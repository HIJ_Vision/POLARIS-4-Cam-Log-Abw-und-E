# POLARIS 4 Cams Application


## Author
Alexander Kessler, a.kessler@hi-jena.gsi.de

## Lizenz
EUPL 1.2

## Installation
- Clone das Projekt, oder lade den Exe Ordner aus Builds herunter.
- Erstelle eine Verknüpfung zur Exe z.B. am Desktop und per Rechtsklick->Eigenschaften übergebe einen beliebigen String an die Exe: z.B.
`...\POLARIS 4 Cam  Log BeamAbw und E.exe.exe" -- "4JustageCams" `
- Dadurch wird eine Datei `4JustageCams.uicnf` angelegt, wo die Einstellungen gespeichert werden
- Wähle die Kameras und den Ordner, wo die Kamerakonfiguration gespeichert wird
- Starte das Kameras
